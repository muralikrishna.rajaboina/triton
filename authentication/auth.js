let passport = require('passport');
let passportJWT = require('passport-jwt');
let cfg = require('../authentication/jwt_config.js');
let ExtractJWT = passportJWT.ExtractJwt;
let Strategy = passportJWT.Strategy;
let params = {
  secretOrKey: cfg.jwtSecret,
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
};
module.exports = function() {
  let strategy = new Strategy(params, function(payload, done) {
    const user = knex(config)
      .from('Users')
      .returning('*')
      .where({ email_id: payload.email_id });
    user.then(u => {
      cfg;
      //console.log(u);
      if (u) {
        return done(null, {
          id: u.id,
          firstname: u.firstname,
          lastname: u.lastname,
          email_id: u.email_id,
          password: u.password,
          role: u.role,
          isActive: u.isActive
        });
      } else {
        return done(new Error('User not found'), null);
      }
    });
  });
  passport.use(strategy);
  return {
    initialize: function() {
      return passport.initialize();
    },
    authenticate: function() {
      return passport.authenticate('jwt', cfg.jwtSession);
    }
  };
};
