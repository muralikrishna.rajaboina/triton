
const test = require('ava');
const request = require('supertest');
const app = require('../app');
const t = require('tcomb-validation');
const ProductionValidate = require('../utils/validations').productionValidate;
// // const validations = require('../utils/validations').validateRequest;
// // const auth = require('../authentication/auth')();

// test('arrays are equal', t => {
// 	t.deepEqual([1, 2], [1, 2]);
// });



test('Production data: Get All', async t => {
  const header = {
    Authorization:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbF9pZCI6ImFkbWluQGdtYWlsLmNvbSIsImlhdCI6MTUzMzUzNzIxNX0.lgL02gXUcKS8FSduUAITlkMgfEYmyk27Xrj-rtn0CLY'
  };

    t.plan(1);
    const res = await request(app)
      .get('/production?pageSize=3&pageIndex=0')
      .set(header);
    t.is(res.status, 200);
  });

  test('Customers: Get By subMaterialId', async t => {
    const header = {
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbF9pZCI6ImFkbWluQGdtYWlsLmNvbSIsImlhdCI6MTUzMzUzNzIxNX0.lgL02gXUcKS8FSduUAITlkMgfEYmyk27Xrj-rtn0CLY'
    };
    
    t.plan(1);
    const res = await request(app)
      .get('/production?pageSize=3&pageIndex=0&subMaterialId=1')
      .set(header);
    t.is(res.status, 200);
  });


const validateRequest = (reqBody, type) => {
  let result = t.validate(reqBody, type);
  return {
    isValid: result.isValid(),
    errors: result.errors.map(x => x.message)
  };
};

  test('Production: Create', async t => {
    const header = {
      Authorization:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbF9pZCI6ImFkbWluQGdtYWlsLmNvbSIsImlhdCI6MTUzMzUzNzIxNX0.lgL02gXUcKS8FSduUAITlkMgfEYmyk27Xrj-rtn0CLY'
    };
    const body = {
      subMaterialId: 9,
      weight: 45.7,
      machineId: 'M9',
      date: '2018-08-06',
      time: '01:30:02',
      
    };
    t.plan(2);
    const res = await request(app)
      .post('/production')
      .set(header)
      .send(body);
    t.is(res.status, 200);
    t.is(validateRequest(res.body,ProductionValidate).isValid,true);
  });
  
  test('Production Bags by Hour: Get All', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/bags/byHour?fromDate=2018-08-06&toDate=2018-08-06&subMaterialId=1')
      .expect([
        {
          hour: 0,
          numberOfBags: 4
        },
        {
          hour: 1,
          numberOfBags: 2
        },
        
        {
          hour: 2,
          numberOfBags: 8
        },
        {
          hour: 3,
          numberOfBags: 4
        },
        {
          hour: 4,
          numberOfBags: 2
        },
        {
          hour: 5,
          numberOfBags: 2
        },
        {
          hour: 6,
          numberOfBags: 3
        },
        {
          hour: 7,
          numberOfBags: 4
        },
        {
          hour: 8,
          numberOfBags: 5
        },
        {
          hour: 9,
          numberOfBags: 4
        },
        {
          hour: 10,
          numberOfBags: 2
        },
        {
          hour: 11,
          numberOfBags: 2
        },
        {
          hour: 12,
          numberOfBags: 4
        },
        {
          hour: 13,
          numberOfBags: 7
        },
        {
          hour: 14,
          numberOfBags: 2
        },
        {
          hour: 16,
          numberOfBags: 3
        },
        {
          hour: 17,
          numberOfBags: 1
        },
        {
          hour: 18,
          numberOfBags: 1
        },
        {
          hour: 19,
          numberOfBags: 3
        },
        
        {
          hour: 21,
          numberOfBags: 4
        },
        {
          hour: 22,
          numberOfBags: 5
        },
        {
          hour: 23,
          numberOfBags: 4
        },
       
        
      ]);
    t.is(res.status, 200);
  });
  
  test('Production Bags by Year: Get All', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/bags/byYear')
      .expect([
        {
            "month": "Jan",
            "numberOfBags": "36433"
        },
        {
            "month": "Feb",
            "numberOfBags": "34013"
        },
        {
            "month": "Mar",
            "numberOfBags": "35778"
        },
        {
            "month": "Apr",
            "numberOfBags": "36840"
        },
        {
            "month": "May",
            "numberOfBags": "38300"
        },
        {
            "month": "Jun",
            "numberOfBags": "36782"
        },
        {
            "month": "Jul",
            "numberOfBags": "33647"
        },
        {
            "month": "Aug",
            "numberOfBags": "7434"
        }
    ]);
    t.is(res.status, 200);
  });
  
  test('Production Bags by Month: Get All', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/bags/byMonth')
      .expect([
        {
            "day": "1st Aug 2018",
            "numberOfBags": "2439"
        },
        {
            "day": "2nd Aug 2018",
            "numberOfBags": "2481"
        },
        {
            "day": "3rd Aug 2018",
            "numberOfBags": "2471"
        },
        {
            "day": "4th Aug 2018",
            "numberOfBags": "2479"
        },
        {
            "day": "5th Aug 2018",
            "numberOfBags": "2454"
        },
        {
            "day": "6th Aug 2018",
            "numberOfBags": "2466"
        },
        {
            "day": "7th Aug 2018",
            "numberOfBags": "2438"
        },
        {
            "day": "8th Aug 2018",
            "numberOfBags": "2476"
        },
        {
            "day": "9th Aug 2018",
            "numberOfBags": "2491"
        },
        {
            "day": "10th Aug 2018",
            "numberOfBags": "1229"
        }
    ]);
    t.is(res.status, 200);
  });
  test('Production Bags by Day: Get All', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/bags/byDay')
      .expect([
        {
            "hour": "11 AM",
            "numberOfBags": "1"
        },
        {
            "hour": "12 PM",
            "numberOfBags": "1"
        },
        {
            "hour": "09 PM",
            "numberOfBags": "2"
        }
    ]);
    t.is(res.status, 200);
  });

  test('All Types of Bags : byMonth', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/allTypes/bags/byMonth')
      .expect({
        "data": [
            {
                "subMaterialId": 1,
                "subMaterial": "CG MAIDA 50KG",
                "count": "5"
            },
            {
                "subMaterialId": 3,
                "subMaterial": "BREAD MAIDA 50KG",
                "count": "2722"
            }
        ],
        "totalCount": "2,727"
    });
    t.is(res.status, 200);
  });

  test('All Types of Bags : byDay', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/allTypes/bags/byDay')
      .expect({
        "data": [
            {
                "subMaterialId": 1,
                "subMaterial": "CG MAIDA 50KG",
                "count": "4"
            }
        ],
        "totalCount": "4"
    });
    t.is(res.status, 200);
  });

  test('Average SubMaterials Production Per Month', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/average/bags/byMonth')
      .expect([
        {
            "subMaterialId": 1,
            "subMaterial": "CG MAIDA 50KG",
            "average": 0.36
        },
        {
            "subMaterialId": 3,
            "subMaterial": "BREAD MAIDA 50KG",
            "average": 194.43
        }
    ]);
    t.is(res.status, 200);
  });

  test('Hourly Average Production Per Month', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/aveHour/bags/byMonth')
      .expect([
        {
            "hour": "05 AM",
            "average": 4.79
        },
        {
            "hour": "07 AM",
            "average": 5.5
        },
        {
            "hour": "08 AM",
            "average": 13.07
        },
        {
            "hour": "09 AM",
            "average": 10.07
        },
        {
            "hour": "10 AM",
            "average": 19.93
        },
        {
            "hour": "11 AM",
            "average": 6.36
        },
        {
            "hour": "12 PM",
            "average": 1.5
        },
        {
            "hour": "05 PM",
            "average": 2.14
        },
        {
            "hour": "06 PM",
            "average": 13.36
        },
        {
            "hour": "07 PM",
            "average": 27.29
        },
        {
            "hour": "08 PM",
            "average": 41.79
        },
        {
            "hour": "09 PM",
            "average": 10.14
        },
        {
            "hour": "10 PM",
            "average": 29.86
        },
        {
            "hour": "11 PM",
            "average": 9
        }
    ]);
    t.is(res.status, 200);
  });

  test('All Weights of bags: by Day', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/allWeights/bags/byDay')
      .expect({
        "data": [
            {
                "subMaterialId": 1,
                "subMaterial": "CG MAIDA 50KG",
                "weight": 160
            }
        ],
        "totalCount": "160"
    });
    t.is(res.status, 200);
  });


  test('Inserting data using GET', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/allWeights/bags/byMonth')
      .expect({
        "data": [
            {
                "subMaterialId": 1,
                "subMaterial": "CG MAIDA 50KG",
                "weight": 200
            },
            {
                "subMaterialId": 3,
                "subMaterial": "BREAD MAIDA 50KG",
                "weight": 265
            },
            {
                "subMaterialId": 4,
                "subMaterial": "CG RAWA 50KG",
                "weight": 101
            },
            {
                "subMaterialId": 5,
                "subMaterial": "BELL RAWA 50KG",
                "weight": 204
            }
        ],
        "totalCount": "770"
    });
    t.is(res.status, 200);
  });

  test.only('All Weights of bags: by Month', async t => {
    t.plan(1);
    const res = await request(app)
      .get('/production/post?subMaterialId=3&weight=50&machineId=k3&date=2018-08-14&time=12:06:00')
     
    t.is(res.status, 204);
  });