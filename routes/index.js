const express = require('express');
const router = express.Router();
const knex = require('knex');
const config = require('../knexfile').development;
const t = require('tcomb-validation');
const validations = require('../utils/validations');
const auth = require('../authentication/auth')();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const LocalStrategy = require('passport-local').Strategy;
const cfg = require('../authentication/jwt_config.js');
const moment = require('moment');
const fs = require('fs');
// const path = require('path');
var jsonexport = require('jsonexport');

const reqBodyValidation = (req, res, next) => {
  let result = t.validate(req.body, validations.validateRequest);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

const reqRoleValidation = (req, res, next) => {
  let result = t.validate(req.body, validations.validateRole);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

router.post('/roles', reqRoleValidation, (req, res) => {
  const db = knex(config);

  db.returning('*')
    .insert({
      role: req.body.roleName,
      isActive: true
    })
    .into('userRoles')
    .then(function(data) {
      db.destroy();
      res.status(200).json({ message: ' role added sucessfully' });
    })
    .catch(e => {
      db.destroy();
      return res.status(500).json({ message: 'role not added' });
    });
});

router.get('/allRoles', (req, res) => {
  const db = knex(config);

  db.select('id', 'role')
    .from('userRoles')
    .where({ isActive: true })
    .then(x => {
      db.destroy();
      return res.json(x);
    })
    .catch(e => {
      return res.status(500).json(e);
    });
});

router.post('/register', reqBodyValidation, (req, res) => {
  const db = knex(config);
  const userNew = db
    .select('email_id')
    .from('Users')
    .where({ isActive: true })
    .andWhere({ email_id: req.body.emailId })
    .first('email_id');

  userNew.then(function(row) {
    if (!row) {
      if (req.body.password === req.body.password2) {
        db.returning('*')
          .insert({
            firstname: req.body.firstName,
            lastname: req.body.lastName,
            email_id: req.body.emailId,
            password: req.body.password,
            role: req.body.roleName,
            isActive: true
          })
          .into('Users')
          .then(function(data) {
            db.destroy();
            res
              .status(200)
              .json({ data: data, message: ' User added sucessfully' });
          })
          .catch(e => {
            db.destroy();
            return res.status(500).json({ message: 'User not added' });
          });
      } else {
        return res.status(400).json({ message: 'Incorrect Password' });
      }
    } else {
      return res.status(400).json({ message: 'EmaiId Already Exists' });
    }
  });
});

const reqloginValidation = (req, res, next) => {
  let result = t.validate(req.body, validations.login);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

router.post('/token', reqloginValidation, async (req, res) => {
  let db;
  try {
    db = knex(config);
    const x = await db
      .where({
        email_id: req.body.emailId,
        password: req.body.password
      })
      .from('Users');
    await db.destroy();
    if (x[0]) {
      return res.json({
        token: jwt.sign({ email_id: req.body.emailId }, cfg.jwtSecret),
        user: x[0]
      });
    } else {
      await db.destroy();
      return res.status(400).json('User not Existed');
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/timeStamp', (req, res) => {
  const date = new Date();

  const values = moment(date)
    .format('DD/MM/YYYY HH:mm:ss ')
    .split(' ');

  const datePart = values[0];
  const timePart = values[1];
  var dateDigits = ('' + datePart).split('/');

  var timeDigits = ('' + timePart).split(':');
  const timeCRC = timeDigits
    .map(x => x.split('').reduce((x, y) => parseInt(x) + parseInt(y)))
    .reduce((x, y) => parseInt(x) + parseInt(y));
  const dateCRC = dateDigits
    .map(x => x.split('').reduce((x, y) => parseInt(x) + parseInt(y)))
    .reduce((x, y) => parseInt(x) + parseInt(y));
  const crc = timeCRC + dateCRC;

  res.removeHeader('X-Powered-By');
  res.removeHeader('Access-Control-Allow-Origin');
  res.removeHeader('Date');
  res.removeHeader('Connection');
  return res.status(200).json({
    timestamp: moment().format('DD/MM/YYYY HH:mm:ss'),
    crc: crc
  });
});

router.get('/datavalues', async (req, res) => {
  const db = knex(config);
  console.log('test');
  const x = await db.select('*').from('productionTracking');
  return res.status(200).json(x);
});

router.post('/postdevicecalibrations', async (req, res) => {
  let db;
  function roundToSix(num) {
    return +(Math.round(num + 'e+6') + 'e-6');
  }
  try {
    db = knex(config);
    console.log('test');
    console.log(req.body);
    const deviceCalibration = roundToSix(
      696 * (req.body.newdeviceWeight / (req.body.olddeviceWeight * 1000))
    );
    console.log(deviceCalibration);
    const calibrationDetails = await db
      .returning(['deviceId', 'deviceCalibration'])
      .insert({
        deviceId: req.body.deviceId,
        deviceCalibration: deviceCalibration
      })
      .into('deviceDetails');

    await db.destroy();
    return res.status(200).json(calibrationDetails[0]);
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});
router.get('/getdevicecalibration', async (req, res) => {
  let db;

  try {
    db = knex(config);
    const calibrationDetails = await db
      .select('deviceId', 'deviceCalibration')
      .from('deviceDetails')
      .where({ deviceId: req.query.deviceId });
    console.log(calibrationDetails);
    await db.destroy();
    return res.status(200).json(calibrationDetails[0]);
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/remotedebug', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);

    const data = await db
      .insert({
        deviceId: req.query.deviceId,
        systemRestartState: req.query.systemRestartState,
        taskNumberState: req.query.taskNumberState,
        stackOverflowstate: req.query.stackOverflowstate,
        hardFault: req.query.hardFault
      })
      .into('remoteDebug');

    await db.destroy();

    return res.json({ message: 'success' });
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/remotedebugdownload', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);

    const data = await db
      .select(
        'deviceId',
        'systemRestartState',
        'taskNumberState',
        'stackOverflowstate',
        'hardFault',
        'created_at'
      )
      .from('remoteDebug')
      .orderBy('created_at', 'desc');

    await db.destroy();
    const logdata = data.map(x => ({
      ...x,
      ...{ created_at: moment(x.created_at).format('YYYY-MM-DD , h:mm:ss a') }
    }));
    console.log(logdata);
    jsonexport(logdata, function(err, csv) {
      if (err) return console.log(err);
      fs.writeFileSync('./uploads/data.csv', csv, 'binary');
    });

    return res.download('./uploads/data.csv', 'data.csv');
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/devicerecordsdownload', async (req, res) => {
  try {
    let db = knex(config);
    var date1 = new Date(req.query.date1);
    var dd1 = date1.getDate();
    date1.setDate(dd1 + 1);
    const middleDate1 = moment(date1)
      .format('YYYY-MM-DD')
      .toString();

    var date2 = new Date(req.query.date2);
    var dd2 = date2.getDate();
    date2.setDate(dd2 - 1);
    const middleDate2 = moment(date2)
      .format('YYYY-MM-DD')
      .toString();

    db = knex(config);
    const data1 = await db
      .select('*')
      .from('productionTracking')
      .where('date', req.query.date1)
      .andWhere('machineId', 'ilike', '%' + req.query.machineId + '%')
      .andWhereBetween('time', [req.query.time1, '23:59:59']);

    const data2 = await db
      .select('*')
      .from('productionTracking')
      .whereBetween('date', [middleDate1, middleDate2])
      .andWhere('machineId', 'ilike', '%' + req.query.machineId + '%');

    const data3 = await db
      .select('*')
      .from('productionTracking')
      .where('date', req.query.date2)
      .andWhere('machineId', 'ilike', '%' + req.query.machineId + '%')
      .andWhereBetween('time', ['00:00:00', req.query.time2]);

    const data = data1.concat(data2).concat(data3);

    await db.destroy();
    const logdata = data.map(x => ({
      ...x,
      ...{ created_at: moment(x.created_at).format('YYYY-MM-DD , h:mm:ss a') }
    }));

    jsonexport(logdata, function(err, csv) {
      if (err) return console.log(err);
      fs.writeFileSync('./uploads/devicedata.csv', csv, 'binary');
    });

    return res.download('./uploads/devicedata.csv', 'devicedata.csv');
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/postdevicevibration', async (req, res) => {
  let db = knex(config);
  try {
    const data = await db
      .insert({
        temperature: req.query.temperature,
        vibration: req.query.vibration,
        date: req.query.date,
        time: req.query.time
      })
      .into('devicevibration');

    await db.destroy();

    return res.json({ message: 'success' });
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/getdevicevibration', async (req, res) => {
  let db = knex(config);
  try {
    const pageSize = parseInt(req.query.pageSize);
    const pageIndex = parseInt(req.query.pageIndex);

    const data = await db
      .select(
        'temperature',
        'vibration',
        db.raw(`to_char(date,'DD-Mon-YYYY') as date`),
        db.raw(`to_char(time,'HH12:MI:SS AM') as time`)
      )
      .from('devicevibration')
      .limit(pageSize)
      .offset(pageSize * pageIndex)
      .orderBy('devicevibration.date', 'desc')
      .orderBy('devicevibration.time', 'desc');

    await db.destroy();

    return res.json(data);
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

module.exports = router;
