const express = require('express');
const router = express.Router();
const knex = require('knex');
const config = require('../knexfile').development;
const t = require('tcomb-validation');
const validations = require('../utils/validations');
const moment = require('moment');
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const path = require('path');

router.get('/weightPercentage/byShift', (req, res) => {
  const db = knex(config);
  const getDay = 'extract(day from date)';
  const getMonth = 'extract(month from date)';
  const getYear = 'extract(year from date)';
  const resolution = ' extract(hour from "time")';
  const d = new Date();
  const currentDay = d.getDate();

  Array.prototype.groupBy = function(prop) {
    return this.reduce(function(groups, item) {
      const val = item[prop];
      groups[val] = groups[val] || [];
      groups[val].push(item);
      return groups;
    }, {});
  };
  function roundToTwo(num) {
    return +(Math.round(num + 'e+2') + 'e-2');
  }
  var date = new Date(req.query.date);
  var dd = date.getDate();
  date.setDate(dd + 1);
  const shiftDate = moment(date)
    .format('YYYY-MM-DD')
    .toString();

  const query1 = db.from(function() {
    this.sum('weight as weight')
      .from('productionTracking')
      .select(
        db.raw(`${resolution} as "hour"`),
        'productionTracking.subMaterialId',
        'subMaterial.subMaterial'
      )
      .where('productionTracking.date', req.query.date)
      .andWhereBetween('productionTracking.time', ['06:00:00', '23:59:59'])
      .andWhere({ 'productionTracking.isActive': true })
      .groupByRaw(resolution)
      .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .innerJoin(
        'subMaterial',
        'subMaterial.id',
        'productionTracking.subMaterialId'
      )
      .as('productionTracking');
  });

  const query2 = db.from(function() {
    this.sum('weight as weight')
      .from('productionTracking')
      .select(
        db.raw(`${resolution} as "hour"`),
        'productionTracking.subMaterialId',
        'subMaterial.subMaterial'
      )
      .where('productionTracking.date', shiftDate)
      .andWhereBetween('productionTracking.time', ['00:00:00', '05:59:59'])
      .andWhere({ 'productionTracking.isActive': true })
      .groupByRaw(resolution)
      .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .innerJoin(
        'subMaterial',
        'subMaterial.id',
        'productionTracking.subMaterialId'
      )
      .as('productionTracking');
  });
  const query3 = db.select('id', 'subMaterial').from('material');
  const promData = Promise.all([query1, query2, query3]);
  promData
    .then(results => {
      db.destroy();

      const shift1Data = [...results[0].filter(it => it.hour < 18)];
      const shift2Data = [
        ...results[0].filter(it => it.hour > 17),
        ...results[1]
      ];

      const shift2 = shift2Data.groupBy('hour');
      const shift1 = shift1Data.groupBy('hour');

      const shift2Keys = [
        ...Object.keys(shift2).filter(it => parseInt(it, 10) >= 18),
        ...Object.keys(shift2).filter(it => parseInt(it, 10) < 18)
      ];

      const shift1TotalHCount = Object.keys(shift1).map(key => ({
        hour: parseInt(key),
        weightcount: shift1[key].reduce(
          (x, y) => x + parseFloat(y.weight, 10),
          0
        )
      }));
      const shift2TotalHCount = Object.keys(shift2).map(key => ({
        hour: parseInt(key),
        weightcount: shift2[key].reduce(
          (x, y) => x + parseFloat(y.weight, 10),
          0
        )
      }));
      const shift1Data2 = shift1Data.map(x => ({
        ...x,
        ...{
          subPercentage: roundToTwo(
            (x.weight /
              shift1TotalHCount
                .filter(it => it.hour === x.hour)
                .map(y => y.weightcount)[0]) *
              100
          )
        }
      }));
      const shift2Data2 = shift2Data.map(x => ({
        ...x,
        ...{
          subPercentage: roundToTwo(
            (x.weight /
              shift2TotalHCount
                .filter(it => it.hour === x.hour)
                .map(y => y.weightcount)[0]) *
              100
          )
        }
      }));

      const shift22 = shift2Data2.groupBy('hour');
      const shift12 = shift1Data2.groupBy('hour');
      const count1 = shift1Data2.groupBy('subMaterialId');
      const count2 = shift2Data2.groupBy('subMaterialId');
      const shift1keys = Object.keys(shift12);
      const shift1Total = Object.keys(count1).map(key => ({
        subMaterialId: parseInt(key),
        avgcount: roundToTwo(
          count1[key].reduce((x, y) => x + parseFloat(y.subPercentage, 10), 0) /
            12
        )
      }));
      const shift2Total = Object.keys(count2).map(key => ({
        subMaterialId: parseInt(key),
        avgcount: roundToTwo(
          count2[key].reduce((x, y) => x + parseFloat(y.subPercentage, 10), 0) /
            12
        )
      }));

      const idvalues = results[2].filter(x => x.subMaterial !== '');
      const shift1TotalAvg = idvalues.map(x => {
        const submaterial = shift1Total.find(it => it.subMaterialId === x.id);
        if (submaterial) {
          return submaterial;
        } else {
          return { subMaterialId: x.id, avgcount: 0 };
        }
      });
      const shift2TotalAvg = idvalues.map(x => {
        const submaterial = shift2Total.find(it => it.subMaterialId === x.id);
        if (submaterial) {
          return submaterial;
        } else {
          return { subMaterialId: x.id, avgcount: 0 };
        }
      });

      const shift1totalGrinding = roundToTwo(
        shift1TotalHCount.reduce((x, y) => x + parseFloat(y.weightcount, 10), 0)
      );
      const shift2totalGrinding = roundToTwo(
        shift2TotalHCount.reduce((x, y) => x + parseFloat(y.weightcount, 10), 0)
      );
      const shift1Grinding = shift1TotalHCount.map(x => ({
        hour: x.hour,
        Grinding: roundToTwo(x.weightcount)
      }));
      shift1Grinding.push({ totalGrinding: shift1totalGrinding });
      const shift2Grinding = shift2TotalHCount.map(x => ({
        hour: x.hour,
        Grinding: roundToTwo(x.weightcount)
      }));
      shift2Grinding.push({ totalGrinding: shift2totalGrinding });
      const branshift1 = shift1keys
        .map(x => shift12[x])
        .map(x => x.filter(y => 5 <= y.subMaterialId && y.subMaterialId <= 11))
        .filter(x => x.length !== 0)
        .map(x => {
          return {
            hour: x[0].hour,
            branweight: x.reduce((x, y) => x + parseFloat(y.weight, 10), 0)
          };
        });

      const branshift1Percentage = shift1Grinding
        .map(t => {
          return {
            hour: t.hour,
            branpercentage: roundToTwo(
              branshift1
                .filter(k => k.hour === t.hour)
                .map(d => d.branweight)[0] === undefined
                ? (0 / t.Grinding) * 100
                : (branshift1
                    .filter(k => k.hour === t.hour)
                    .map(d => d.branweight)[0] /
                    t.Grinding) *
                    100
            )
          };
        })
        .filter(x => x.hour !== undefined);

      const branshift2 = shift2Keys
        .map(x => shift22[x])
        .map(x => x.filter(y => 5 <= y.subMaterialId && y.subMaterialId <= 11))
        .filter(x => x.length !== 0)
        .map(x => {
          return {
            hour: x[0].hour,
            branweight: x.reduce((x, y) => x + parseFloat(y.weight, 10), 0)
          };
        });

      const branshift2Percentage = shift2Grinding
        .map(t => {
          return {
            hour: t.hour,
            branpercentage: roundToTwo(
              branshift2
                .filter(k => k.hour === t.hour)
                .map(d => d.branweight)[0] === undefined
                ? (0 / t.Grinding) * 100
                : (branshift2
                    .filter(k => k.hour === t.hour)
                    .map(d => d.branweight)[0] /
                    t.Grinding) *
                    100
            )
          };
        })
        .filter(x => x.hour !== undefined);

      const branshift1PercentageTotal = roundToTwo(
        branshift1Percentage.reduce(
          (x, y) => x + parseFloat(y.branpercentage, 10),
          0
        ) / 12
      );
      const branshift2PercentageTotal = roundToTwo(
        branshift2Percentage.reduce(
          (x, y) => x + parseFloat(y.branpercentage, 10),
          0
        ) / 12
      );

      return res.status(200).json({
        shift1: shift1keys.map(x => shift12[x]),
        shift2: shift2Keys.map(x => shift22[x]),
        shift1TotalAvg: shift1TotalAvg.sort(function(a, b) {
          return a.subMaterialId - b.subMaterialId;
        }),
        shift2TotalAvg: shift2TotalAvg.sort(function(a, b) {
          return a.subMaterialId - b.subMaterialId;
        }),
        shift1Grinding: shift1Grinding,
        shift2Grinding: shift2Grinding,
        branshift1: [
          ...branshift1Percentage,
          { totalBran: branshift1PercentageTotal }
        ],
        branshift2: [
          ...branshift2Percentage,
          { totalBran: branshift2PercentageTotal }
        ]
      });
    })
    .catch(e => {
      db.destroy();
      console.log(e);
      return res.status(500).json(e);
    });
});

module.exports = router;
