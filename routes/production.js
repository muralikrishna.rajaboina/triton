const express = require('express');
const router = express.Router();
const knex = require('knex');
const config = require('../knexfile').development;
const t = require('tcomb-validation');
const validations = require('../utils/validations');
const moment = require('moment');
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const asyncErrorHandlerMiddleWare = require('../utils/async_custom_handlers')
  .asyncErrorHandler;
Array.prototype.groupBy = function(prop) {
  return this.reduce(function(groups, item) {
    const val = item[prop];
    groups[val] = groups[val] || [];
    groups[val].push(item);
    return groups;
  }, {});
};
const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    console.log('storage');
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    console.log('storage');

    cb(
      null,
      file.fieldname +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});

const upload = multer({
  //multer settings

  storage: storage,
  limits: {
    fileSize: 52428800
  },
  fileFilter: function(req, file, callback) {
    //file filter
    console.log('check file extension');
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('production');

router.post('/data', function(req, res) {
  console.log('xlsx to json conversion');
  req.setTimeout(0);
  let exceltojson;
  upload(req, res, function(err) {
    if (err) {
      res.status(400).json({ error_code: 1, err_desc: err });
      return;
    }

    if (!req.file) {
      res.status(400).json({ error_code: 1, err_desc: 'No file passed' });
      return;
    }

    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }

    try {
      exceltojson(
        {
          input: req.file.path, //the same path where we uploaded our file
          output: null, //since we don't need output.json
          lowerCaseHeaders: false
        },
        function(err, result) {
          if (err) {
            return res
              .status(400)
              .json({ error_code: 1, err_desc: err, data: null });
          }

          const result1 = result
            .filter(o => o['weight'] != null && o['weight'] != '')
            .map(o => ({
              subMaterialId: o['subMaterialId'],
              weight: o['weight'],
              machineId: o['machineId'],
              uniqueProductId: o['uniqueProductId'],
              date: o['date'],
              time: o['time'],
              isActive: true,
              created_at: new Date().toISOString(),
              updated_at: new Date().toISOString()
            }));
          const db = knex(config);
          db.transaction(function(tr) {
            return knex(config)
              .batchInsert('productionTracking', result1)
              .transacting(tr);
          })
            .then(() => {
              db.destroy();
              return res.status(200).json({ message: 'success' });
            })
            .catch(error => {
              db.destroy();
              return res.status(400).json({ message: error.message });
            });
        }
      );
    } catch (e) {
      db.destroy();
      res.status(400).json({ error_code: 1, err_desc: 'Corrupted excel file' });
    }
  });
});

const reqProductionValidation = (req, res, next) => {
  let result = t.validate(req.body, validations.productionValidate);
  if (result.isValid()) next();
  else res.status(400).json(result.errors.map(x => x.message));
};

router.post('/', reqProductionValidation, (req, res) => {
  let db = knex(config);
  const times = moment().format('x');
  const getTimestamps = times.toString();
  db.insert({
    subMaterialId: req.body.subMaterialId,
    weight: req.body.weight,
    machineId: req.body.machineId,
    uniqueProductId: req.body.machineId + getTimestamps,
    date: req.body.date,
    time: req.body.time,
    isActive: true
  })
    .into('productionTracking')
    .returning('*')
    .then(pt => {
      db.destroy();
      db('subMaterial')
        .select('subMaterial')
        .where('id', pt[0].subMaterialId)
        .then(x => {
          db.destroy();
          return res
            .status(200)
            .json({ ...pt[0], subMaterial: x[0].subMaterial });
        });
    })
    .catch(e => {
      db.destroy();
      return res.status(500).json(e);
    });
});

const reqQueryValidation = (req, res, next) => {
  const result = t.validate(req.query, validations.pagination);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

router.get('/', reqQueryValidation, (req, res) => {
  const db = knex(config);
  const pageSize = parseInt(req.query.pageSize);
  const pageIndex = parseInt(req.query.pageIndex);

  const query = db
    .select(
      'productionTracking.id ',
      'subMaterialId',
      'subMaterial.subMaterial',
      'weight',
      'productionTracking.machineId',
      'uniqueProductId',
      db.raw(`to_char(date,'DD-Mon-YYYY') as date`),
      db.raw(`to_char(time,'HH12:MI:SS AM') as time`),
      'productionTracking.isActive'
    )
    .from('productionTracking')
    .where({ 'productionTracking.isActive': true })
    .limit(pageSize)
    .offset(pageSize * pageIndex)
    .innerJoin(
      'subMaterial',
      'subMaterial.id',
      'productionTracking.subMaterialId'
    )
    .orderBy('productionTracking.date', 'desc')
    .orderBy('productionTracking.time', 'desc');

  const Count = db('productionTracking').count('id');

  if (req.query.subMaterialId && req.query.subMaterialId !== '') {
    query.where('subMaterialId', req.query.subMaterialId);
    Count.where('subMaterialId', req.query.subMaterialId);
  }

  if (req.query.date && req.query.date !== '') {
    query.where('date', req.query.date);
    Count.where('date', req.query.date);
  }

  if (req.query.weight && req.query.weight !== '') {
    query.whereBetween('weight', [40.5, req.query.weight]);
    Count.where('weight', req.query.weight);
  }
  const promData = Promise.all([query, Count])
    .then(results => {
      db.destroy();

      return res.json({
        data: results[0],
        count: parseInt(results[1][0].count)
      });
    })
    .catch(e => {
      db.destroy();
      return res.status(500).json(e);
    });
});

const reqGraphValidation = (req, res, next) => {
  const result = t.validate(req.query, validations.graphValidate);
  if (result.isValid()) next();
  else res.status(400).json(result.errors);
};

router.get(
  '/bags/byHour',
  reqGraphValidation,
  asyncErrorHandlerMiddleWare(async (req, res, next) => {
    let db = knex(config);
    try {
      db = knex(config);
      const fromDate = req.query.fromDate;
      const toDate = req.query.toDate;
      const subMaterialId = parseInt(req.query.subMaterialId);

      const resolution = ' extract(hour from "time")';

      var fromday = new Date(req.query.fromDate);

      var today = new Date(req.query.toDate);

      const shiftfromDate = moment(
        fromday.setDate(fromday.getDate() + 1)
      ).format('YYYY-MM-DD');
      const shifttoDate = moment(today.setDate(today.getDate() + 1)).format(
        'YYYY-MM-DD'
      );
      const shift1 = await db
        .select(
          db.raw(
            `${resolution} as "hour", count(distinct "id") as "numberOfBags"`
          )
        )
        .from('productionTracking')
        .whereBetween('date', [fromDate, toDate])
        .andWhereBetween('time', ['06:00:00', '23:59:59'])
        .andWhere({ subMaterialId: subMaterialId })
        .andWhere({ isActive: true })
        .groupByRaw(resolution);

      const shift2 = await db
        .select(
          db.raw(
            `${resolution} as "hour", count(distinct "id") as "numberOfBags"`
          )
        )
        .from('productionTracking')
        .whereBetween('date', [shiftfromDate, shifttoDate])
        .andWhereBetween('time', ['00:00:00', '05:59:59'])
        .andWhere({ subMaterialId: subMaterialId })
        .andWhere({ isActive: true })
        .groupByRaw(resolution);

      if (shift1) {
        await db.destroy();
        const shift1Data = shift1.map(x => ({
          hour: moment(x.hour, ['HH']).format('hh A'),
          numberOfBags: x.numberOfBags
        }));
        const shift2Data = shift2.map(x => ({
          hour: moment(x.hour, ['HH']).format('hh A'),
          numberOfBags: x.numberOfBags
        }));
        const data = shift1Data.concat(shift2Data);
        return res.status(200).json(data);
      }
    } catch (error) {
      await db.destroy();
      return res.status(500).json({
        message: 'some thing awefull happened'
      });
    }
  })
);

const reqBagsCurrentDayValidation = (req, res, next) => {
  let result = t.validate(req.query, validations.bagsCurrentDayValidate);
  if (result.isValid()) next();
  else res.status(400).json(result.errors.map(x => x.message));
};
router.get('/bags/currentDay', reqBagsCurrentDayValidation, (req, res) => {
  const db = knex(config);
  const subMaterialId = parseInt(req.query.subMaterialId);
  const resolution = ' extract(hour from "time")';

  const d = new Date();

  const currentDay = d.getDate();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());
  const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;
  var nextDay = new Date(currentDate);
  const tomorrow = moment(nextDay.setDate(nextDay.getDate() + 1)).format(
    'YYYY-MM-DD'
  );
  console.log(moment(tomorrow).format('YYYY-MM-DD'));

  const query = db
    .select(
      db.raw(`${resolution} as "hour", count(distinct "id") as "numberOfBags"`)
    )
    .from('productionTracking')
    .where('date', currentDate)
    .andWhereBetween('time', ['06:00:00', '23:59:59'])
    .andWhere({ subMaterialId: subMaterialId })
    .andWhere({ isActive: true })
    .groupByRaw(resolution);

  const query1 = db
    .select(
      db.raw(`${resolution} as "hour", count(distinct "id") as "numberOfBags"`)
    )
    .from('productionTracking')
    .where('date', tomorrow)
    .andWhereBetween('time', ['00:00:00', '05:59:59'])
    .andWhere({ subMaterialId: subMaterialId })
    .andWhere({ isActive: true })
    .groupByRaw(resolution);

  const promData = Promise.all([query, query1]);
  promData
    .then(results => {
      db.destroy();

      const data = results[0].concat(results[1]);
      const Count = data.reduce((x, y) => x + parseInt(y.numberOfBags), 0);
      return res.status(200).json({
        data: data.map(x => ({
          hour: moment(x.hour, ['HH']).format('hh A'),
          numberOfBags: x.numberOfBags
        })),
        totalCount: Count.toLocaleString()
      });
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

router.get('/bags/byYear', (req, res) => {
  const db = knex(config);

  const resolution = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const currentYear = parseInt(new Date().getFullYear());

  db.select(
    db.raw(`${resolution} as "month", count(distinct "id") as "numberOfBags"`)
  )
    .where(db.raw(`${getYear}`), currentYear)
    .andWhere({ isActive: true })
    .from('productionTracking')
    .groupByRaw(resolution)
    .then(x => {
      db.destroy();
      const data = x.map(x => ({
        month: moment(x.month, ['MM']).format('MMM'),
        numberOfBags: x.numberOfBags
      }));
      return res.status(200).json({
        data: data,
        totalCount: data
          .reduce((x, y) => parseInt(x) + parseInt(y.numberOfBags), 0)
          .toLocaleString()
      });
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

router.get('/bags/byShift', (req, res) => {
  const db = knex(config);

  const resolution = ' extract(hour from "time" ) ';

  const d = new Date();

  const currentDay = d.getDate();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());
  const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;

  var nextDay = new Date(currentDate);
  const tomorrow = moment(nextDay.setDate(nextDay.getDate() + 1)).format(
    'YYYY-MM-DD'
  );
  console.log(moment(tomorrow).format('YYYY-MM-DD'));
  var n = d.getHours();

  //const hours=Array.from(Array(n+1).keys())

  const query = db
    .select(
      db.raw(`${resolution} as "hour", count(distinct "id") as "numberOfBags"`)
    )
    .from('productionTracking')
    .where('date', currentDate)
    .andWhereBetween('time', ['06:00:00', '23:59:59'])
    .andWhere({ isActive: true })
    .groupByRaw(resolution);

  const query1 = db
    .select(
      db.raw(`${resolution} as "hour", count(distinct "id") as "numberOfBags"`)
    )
    .from('productionTracking')
    .where('date', tomorrow)
    .andWhereBetween('time', ['00:00:00', '05:59:59'])
    .andWhere({ isActive: true })
    .groupByRaw(resolution);

  const promData = Promise.all([query, query1]);
  promData
    .then(results => {
      db.destroy();
      const shift1 = results[0]
        .filter(x => x.hour <= 17)
        .map(y => ({
          hour: moment(y.hour, ['HH']).format('hh A'),
          numberOfBags: y.numberOfBags
        }));
      const shift2 = results[0]
        .filter(x => x.hour > 17)
        .concat(results[1])
        .map(y => ({
          hour: moment(y.hour, ['HH']).format('hh A'),
          numberOfBags: y.numberOfBags
        }));

      return res.status(200).json({
        shift1: shift1,
        shift2: shift2
      });
    })
    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

router.get('/allTypes/bags/byMonth', (req, res) => {
  const db = knex(config);

  const getMonth = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const d = new Date();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());

  function daysInMonth(currentMonth, currentYear) {
    return new Date(currentYear, currentMonth, 0).getDate();
  }

  const Count = db('productionTracking')
    .where({ isActive: true })
    .andWhere(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .count('id');

  const query = db
    .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .where({ 'productionTracking.isActive': true })
    .andWhere(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .from('productionTracking')
    .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .count('subMaterialId')
    .innerJoin(
      'subMaterial',
      'subMaterial.id',
      'productionTracking.subMaterialId'
    );

  const promData = Promise.all([query, Count])
    .then(results => {
      db.destroy();

      return res.json({
        data: results[0],
        totalCount: parseInt(results[1][0].count).toLocaleString()
      });
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

router.get('/allTypes/bags/byDay', async (req, res) => {
  const db = knex(config);
  const getDay = 'extract(day from "date")';
  const getMonth = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const d = new Date();

  const currentDay = d.getDate();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());
  const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;

  var nextDay = new Date(currentDate);
  const tomorrow = moment(nextDay.setDate(nextDay.getDate() + 1)).format(
    'YYYY-MM-DD'
  );

  const Count1 = await db('productionTracking')
    .where({ isActive: true })
    .where('date', currentDate)
    .andWhereBetween('time', ['06:00:00', '23:59:59'])
    .count('id');
  const query1 = await db
    .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .where({ 'productionTracking.isActive': true })
    .where('date', currentDate)
    .andWhereBetween('time', ['06:00:00', '23:59:59'])
    .from('productionTracking')
    // .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    // .count('subMaterialId')
    .innerJoin(
      'subMaterial',
      'subMaterial.id',
      'productionTracking.subMaterialId'
    );

  const query2 = await db
    .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .where({ 'productionTracking.isActive': true })
    .where('date', tomorrow)
    .andWhereBetween('time', ['00:00:00', '05:59:59'])
    .from('productionTracking')
    // .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    // .count('subMaterialId')
    .innerJoin(
      'subMaterial',
      'subMaterial.id',
      'productionTracking.subMaterialId'
    );

  if (query1) {
    await db.destroy();
    const data = query1.concat(query2);
    const groupedData = data.groupBy('subMaterialId');

    const keys = Object.keys(groupedData);
    const newData = keys.map(x => ({
      subMaterialId: x,
      subMaterial: groupedData[x][0].subMaterial,
      count: groupedData[x].length
    }));
    return res.json({
      data: newData,
      totalCount: data.length

      //   data: results[0],
      //   totalCount: parseInt(results[1][0].count).toLocaleString()
    });
  } else {
    await db.destroy();

    return res.status(500).json(e);
  }
});

router.get('/average/bags/byMonth', (req, res) => {
  const db = knex(config);
  const resolution = 'extract(hour from "time")';
  const getMonth = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const d = new Date();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());

  function daysInMonth(currentMonth, currentYear) {
    return new Date(currentYear, currentMonth, 0).getDate();
  }
  function roundToTwo(num) {
    return +(Math.round(num + 'e+2') + 'e-2');
  }
  const currentDay = d.getDate();

  const percentage = db
    .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .where({ 'productionTracking.isActive': true })
    .andWhere(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .from('productionTracking')
    .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .count('subMaterialId')
    .innerJoin(
      'subMaterial',
      'subMaterial.id',
      'productionTracking.subMaterialId'
    )
    .then(x => {
      db.destroy();
      return res.status(200).json(
        x.map(x => ({
          subMaterialId: x.subMaterialId,
          subMaterial: x.subMaterial,
          average: roundToTwo(x.count / currentDay)
        }))
      );
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});
router.get('/aveHour/bags/byMonth', (req, res) => {
  const db = knex(config);

  const resolution = ' extract(hour from "time")';
  const getMonth = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const d = new Date();
  const currentDay = d.getDate();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());
  function roundToTwo(num) {
    return +(Math.round(num + 'e+2') + 'e-2');
  }
  db.select(db.raw(`${resolution} as "hour"`))
    .from('productionTracking')
    .where({ 'productionTracking.isActive': true })
    .andWhere(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .groupByRaw(resolution)
    .count('productionTracking.id')
    .then(x => {
      db.destroy();
      return res.status(200).json(
        x.map(x => ({
          hour: moment(x.hour, ['HH']).format('hh A'),
          average: roundToTwo(x.count / currentDay)
        }))
      );
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

router.get('/allWeights/bags/byDay', (req, res) => {
  const db = knex(config);
  const getDay = 'extract(day from date)';
  const getMonth = 'extract(month from date)';
  const getYear = 'extract(year from date)';

  const d = new Date();
  const currentDay = d.getDate();

  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());

  function daysInMonth(currentMonth, currentYear) {
    return new Date(currentYear, currentMonth, 0).getDate();
  }
  function roundToTwo(num) {
    return +(Math.round(num + 'e+2') + 'e-2');
  }
  const totalWeight = db.from(function() {
    this.sum('weight as weight')
      .from('productionTracking')
      .where({ isActive: true })
      .andWhere(db.raw(`${getYear}`), currentYear)
      .andWhere(db.raw(`${getMonth}`), currentMonth)
      .andWhere(db.raw(`${getDay}`), currentDay)
      .as('productionTracking');
  });
  const query = db.from(function() {
    this.sum('weight as weight')
      .from('productionTracking')
      .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .where({ 'productionTracking.isActive': true })
      .andWhere(db.raw(`${getYear}`), currentYear)
      .andWhere(db.raw(`${getMonth}`), currentMonth)
      .andWhere(db.raw(`${getDay}`), currentDay)
      .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .innerJoin(
        'subMaterial',
        'subMaterial.id',
        'productionTracking.subMaterialId'
      )
      .as('productionTracking');
  });
  const promData = Promise.all([query, totalWeight]);
  promData
    .then(results => {
      db.destroy();

      return res.json({
        data: results[0].map(x => ({
          subMaterialId: x.subMaterialId,
          subMaterial: x.subMaterial,
          weight: roundToTwo(x.weight)
        })),
        totalCount: parseInt(results[1][0].weight).toLocaleString()
      });
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});
router.get('/allWeights/bags/byMonth', (req, res) => {
  const db = knex(config);

  const getMonth = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const d = new Date();

  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());

  function daysInMonth(currentMonth, currentYear) {
    return new Date(currentYear, currentMonth, 0).getDate();
  }
  function roundToTwo(num) {
    return +(Math.round(num + 'e+2') + 'e-2');
  }
  const totalWeight = db.from(function() {
    this.sum('weight as weight')
      .from('productionTracking')
      .where({ isActive: true })
      .andWhere(db.raw(`${getYear}`), currentYear)
      .andWhere(db.raw(`${getMonth}`), currentMonth)
      .as('productionTracking');
  });
  const query = db.from(function() {
    this.sum('weight as weight')
      .from('productionTracking')
      .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .where({ 'productionTracking.isActive': true })
      .andWhere(db.raw(`${getYear}`), currentYear)
      .andWhere(db.raw(`${getMonth}`), currentMonth)
      .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .innerJoin(
        'subMaterial',
        'subMaterial.id',
        'productionTracking.subMaterialId'
      )
      .as('productionTracking');
  });
  const promData = Promise.all([query, totalWeight]);
  promData
    .then(results => {
      db.destroy();

      return res.json({
        data: results[0].map(x => ({
          subMaterialId: x.subMaterialId,
          subMaterial: x.subMaterial,
          weight: roundToTwo(x.weight)
        })),
        totalWeight: parseInt(results[1][0].weight).toLocaleString()
      });
    })
    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

router.get('/percentage/bags/byMonth', (req, res) => {
  const db = knex(config);
  const resolution = 'extract(hour from "time")';
  const getMonth = 'extract(month from "date")';
  const getYear = 'extract(year from "date")';

  const d = new Date();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());

  function daysInMonth(currentMonth, currentYear) {
    return new Date(currentYear, currentMonth, 0).getDate();
  }
  function roundToTwo(num) {
    return +(Math.round(num + 'e+2') + 'e-2');
  }
  const currentDay = d.getDate();

  const Count = db('productionTracking')
    .where({ isActive: true })
    .andWhere(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .count('id');
  const query = db
    .select(
      'productionTracking.subMaterialId',
      'subMaterial.subMaterial',
      db.raw(`count("subMaterialId") as subCount`)
    )
    .where({ 'productionTracking.isActive': true })
    .andWhere(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .from('productionTracking')
    .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
    .innerJoin(
      'subMaterial',
      'subMaterial.id',
      'productionTracking.subMaterialId'
    );

  const promData = Promise.all([query, Count])
    .then(results => {
      const totalCount = parseInt(results[1][0].count);
      db.destroy();

      return res.json({
        data: results[0].map(x => ({
          id: x.subMaterialId,
          subMaterial: x.subMaterial,
          subcount: x.subcount
        })),
        totalCount: totalCount
      });
    })
    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

const reqProductionGetValidation = (req, res, next) => {
  let result = t.validate(req.query, validations.productionGetValidate);
  if (result.isValid()) next();
  else res.status(400).json(result.errors.map(x => x.message));
};

const reqProductionDelete = (req, res, next) => {
  let result = t.validate(req.params, validations.deleteValidate);
  if (result.isValid()) next();
  else res.status(400).json(result.errors.map(x => x.message));
};

router.delete('/:id', reqProductionDelete, (req, res) => {
  const db = knex(config);
  const deleteCourse = db
    .from('productionTracking')
    .update({ isActive: false })
    .where({ id: req.params.id })
    .then(() => {
      db.destroy();
      return res.status(200).json({ message: 'record deleted by Admin' });
    })
    .catch(error => {
      db.destroy();
      return res.status(500).json({ message: 'record not deleted' });
    });
});

//data insert service get call
router.get('/post', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);
    const times = moment().format('x');
    const getTimestamps = times.toString();
    const d = new Date();

    function isNumeric(num) {
      return !isNaN(num);
    }
    const subMateriaList = await db
      .select('materialId', 'machineId')
      .from('subMaterial')
      .where('isActive', true);

    const subMaterialId = subMateriaList.filter(
      x => x.machineId === req.query.machineId
    );

    // if(isNumeric(req.query.weight) && 22<=parseFloat(req.query.weight)/1000 &&
    //  parseFloat(req.query.weight) /1000<=85){

    if (isNumeric(req.query.weight)) {
      const data = await db
        .insert({
          subMaterialId: parseInt(subMaterialId[0].materialId),
          weight: parseInt(req.query.weight) / 1000,
          machineId: req.query.machineId,
          uniqueProductId: req.query.machineId + getTimestamps,
          date: req.query.date,
          time: req.query.time,
          isActive: true
        })
        .into('productionTracking')
        .returning('*');

      await db.destroy();
      if (data[0]) {
        res.removeHeader('X-Powered-By');
        res.removeHeader('Access-Control-Allow-Origin');
        res.removeHeader('Etag');
        res.removeHeader('Date');
        res.removeHeader('Connection');
        return res.status(200).json({ message: 'success' });
      }
    } else {
      await db
        .insert({
          log:
            req.query.weight +
            ',' +
            req.query.machineId +
            ',' +
            req.query.date +
            ',' +
            req.query.time
        })
        .into('logTracking');

      await db.destroy();
      return res.status(200).json({ message: 'success' });
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

// total bags produced in currentday and yesterday by shif1 and shift2
router.get('/bags/shifts', (req, res) => {
  const db = knex(config);

  const resolution = ' extract(hour from "time" ) ';

  const d = new Date();

  const currentDay = d.getDate();
  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());
  const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;

  var lastday = new Date(currentDate);
  var futureday = new Date(currentDate);
  const yesterday = moment(lastday.setDate(lastday.getDate() - 1)).format(
    'YYYY-MM-DD'
  );
  const tomorrow = moment(futureday.setDate(futureday.getDate() + 1)).format(
    'YYYY-MM-DD'
  );

  let shift2Date = tomorrow;
  if (req.query.shiftDate === 'today') {
    shift1Date = currentDate;
    shift2Date = tomorrow;
  }
  if (req.query.shiftDate === 'yesterday') {
    shift1Date = yesterday;
    shift2Date = currentDate;
  }
  console.log(req.query.shiftDate);
  console.log(shift1Date, shift2Date);

  const query = db
    .select(
      db.raw(`${resolution} as "hour", count(distinct "id") as "numberOfBags"`)
    )
    .from('productionTracking')
    .where('date', shift1Date)
    .andWhereBetween('time', ['06:00:00', '23:59:59'])
    .andWhere({ isActive: true })
    .groupByRaw(resolution);

  const query1 = db
    .select(
      db.raw(`${resolution} as "hour", count(distinct "id") as "numberOfBags"`)
    )
    .from('productionTracking')
    .where('date', shift2Date)
    .andWhereBetween('time', ['00:00:00', '05:59:59'])
    .andWhere({ isActive: true })
    .groupByRaw(resolution);

  const promData = Promise.all([query, query1]);
  promData
    .then(results => {
      const shift1 = results[0]
        .filter(x => x.hour <= 17)
        .map(y => ({
          hour: moment(y.hour, ['HH']).format('hh A'),
          numberOfBags: y.numberOfBags
        }));
      const shift2 = results[0]
        .filter(x => x.hour > 17)
        .concat(results[1])
        .map(y => ({
          hour: moment(y.hour, ['HH']).format('hh A'),
          numberOfBags: y.numberOfBags
        }));

      return res.status(200).json({
        shift1: shift1,
        shift2: shift2
      });
    })
    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

//bags produced by today and yesterday in every hour
router.get('/bags/byDay', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);
    const resolution = ' extract(hour from "time" ) ';
    const d = new Date();
    const currentDay = d.getDate();
    const currentMonth = parseInt(d.getMonth()) + 1;
    const currentYear = parseInt(d.getFullYear());
    const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;
    var lastday = new Date(currentDate);
    var futureday = new Date(currentDate);
    const yesterday = moment(lastday.setDate(lastday.getDate() - 1)).format(
      'YYYY-MM-DD'
    );
    const tomorrow = moment(futureday.setDate(futureday.getDate() + 1)).format(
      'YYYY-MM-DD'
    );
    let shift1Date = currentDate;
    let shift2Date = tomorrow;
    if (req.query.shiftDate === 'today') {
      shift1Date = currentDate;
      shift2Date = tomorrow;
    }
    if (req.query.shiftDate === 'yesterday') {
      shift1Date = yesterday;
      shift2Date = currentDate;
    }
    const query1 = await db
      .select(
        db.raw(
          `${resolution} as "hour", count(distinct "id") as "numberOfBags"`
        )
      )
      .from('productionTracking')
      .where('date', shift1Date)
      .andWhereBetween('time', ['06:00:00', '23:59:59'])
      .andWhere({ isActive: true })
      .groupByRaw(resolution);

    const query2 = await db
      .select(
        db.raw(
          `${resolution} as "hour", count(distinct "id") as "numberOfBags"`
        )
      )
      .from('productionTracking')
      .where('date', shift2Date)
      .andWhereBetween('time', ['00:00:00', '05:59:59'])
      .andWhere({ isActive: true })
      .groupByRaw(resolution);

    if (query1) {
      await db.destroy();
      const data = query1.concat(query2);
      return res.status(200).json({
        data: data.map(x => ({
          hour: moment(x.hour, ['HH']).format('hh A'),
          numberOfBags: x.numberOfBags
        })),
        totalCount: data.reduce(
          (x, y) => parseInt(x) + parseInt(y.numberOfBags),
          0
        )
      });
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

//number of bags produced in currentday and previouday by particular category
router.get('/bags/category', reqBagsCurrentDayValidation, async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);
    const subMaterialId = parseInt(req.query.subMaterialId);
    const resolution = ' extract(hour from "time")';

    const d = new Date();

    const currentDay = d.getDate();
    const currentMonth = parseInt(d.getMonth()) + 1;
    const currentYear = parseInt(d.getFullYear());
    const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;

    var lastday = new Date(currentDate);
    var futureday = new Date(currentDate);
    const yesterday = moment(lastday.setDate(lastday.getDate() - 1)).format(
      'YYYY-MM-DD'
    );
    const tomorrow = moment(futureday.setDate(futureday.getDate() + 1)).format(
      'YYYY-MM-DD'
    );

    let shift1Date = currentDate;
    let shift2Date = tomorrow;
    if (req.query.shiftDate === 'today') {
      shift1Date = currentDate;
      shift2Date = tomorrow;
    }
    if (req.query.shiftDate === 'yesterday') {
      shift1Date = yesterday;
      shift2Date = currentDate;
    }

    const query1 = await db
      .select(
        db.raw(
          `${resolution} as "hour", count(distinct "id") as "numberOfBags"`
        )
      )
      .from('productionTracking')
      .where('date', shift1Date)
      .andWhereBetween('time', ['06:00:00', '23:59:59'])
      .andWhere({ subMaterialId: subMaterialId })
      .andWhere({ isActive: true })
      .groupByRaw(resolution);

    const query2 = await db
      .select(
        db.raw(
          `${resolution} as "hour", count(distinct "id") as "numberOfBags"`
        )
      )
      .from('productionTracking')
      .where('date', shift2Date)
      .andWhereBetween('time', ['00:00:00', '05:59:59'])
      .andWhere({ subMaterialId: subMaterialId })
      .andWhere({ isActive: true })
      .groupByRaw(resolution);
    if (query1) {
      await db.destroy();
      const data = query1.concat(query2);
      const Count = data.reduce((x, y) => x + parseInt(y.numberOfBags), 0);
      return res.status(200).json({
        data: data.map(x => ({
          hour: moment(x.hour, ['HH']).format('hh A'),
          numberOfBags: x.numberOfBags
        })),
        totalCount: Count.toLocaleString()
      });
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

//all types of bags by currentday and previousday (each category)
router.get('/allcategory/bags', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);
    const d = new Date();

    const currentDay = d.getDate();
    const currentMonth = parseInt(d.getMonth()) + 1;
    const currentYear = parseInt(d.getFullYear());
    const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;

    var lastday = new Date(currentDate);
    var futureday = new Date(currentDate);
    const yesterday = moment(lastday.setDate(lastday.getDate() - 1)).format(
      'YYYY-MM-DD'
    );
    const tomorrow = moment(futureday.setDate(futureday.getDate() + 1)).format(
      'YYYY-MM-DD'
    );

    let shift1Date = currentDate;
    let shift2Date = tomorrow;
    if (req.query.shiftDate === 'today') {
      shift1Date = currentDate;
      shift2Date = tomorrow;
    }
    if (req.query.shiftDate === 'yesterday') {
      shift1Date = yesterday;
      shift2Date = currentDate;
    }

    const query1 = await db
      .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .where({ 'productionTracking.isActive': true })
      .where('date', shift1Date)
      .andWhereBetween('time', ['06:00:00', '23:59:59'])
      .from('productionTracking')
      .innerJoin(
        'subMaterial',
        'subMaterial.id',
        'productionTracking.subMaterialId'
      );

    const query2 = await db
      .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
      .where({ 'productionTracking.isActive': true })
      .where('date', shift2Date)
      .andWhereBetween('time', ['00:00:00', '05:59:59'])
      .from('productionTracking')
      .innerJoin(
        'subMaterial',
        'subMaterial.id',
        'productionTracking.subMaterialId'
      );

    if (query1) {
      await db.destroy();
      const data = query1.concat(query2);
      const groupedData = data.groupBy('subMaterialId');

      const keys = Object.keys(groupedData);
      const newData = keys.map(x => ({
        subMaterialId: x,
        subMaterial: groupedData[x][0].subMaterial,
        count: groupedData[x].length
      }));
      return res.json({
        data: newData,
        totalCount:
          data.length.toLocaleString() === 'NaN'
            ? 0
            : data.length.toLocaleString()
      });
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

//all types of bags total weights by currentday and previousday (each category)
router.get('/allcategoryWeights', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);
    const d = new Date();

    const currentDay = d.getDate();
    const currentMonth = parseInt(d.getMonth()) + 1;
    const currentYear = parseInt(d.getFullYear());
    const currentDate = currentYear + '-' + currentMonth + '-' + currentDay;

    var lastday = new Date(currentDate);
    var futureday = new Date(currentDate);
    const yesterday = moment(lastday.setDate(lastday.getDate() - 1)).format(
      'YYYY-MM-DD'
    );
    const tomorrow = moment(futureday.setDate(futureday.getDate() + 1)).format(
      'YYYY-MM-DD'
    );

    let shift1Date = currentDate;
    let shift2Date = tomorrow;
    if (req.query.shiftDate === 'today') {
      shift1Date = currentDate;
      shift2Date = tomorrow;
    }
    if (req.query.shiftDate === 'yesterday') {
      shift1Date = yesterday;
      shift2Date = currentDate;
    }

    function roundToTwo(num) {
      return +(Math.round(num + 'e+2') + 'e-2');
    }

    const query1 = await db.from(function() {
      this.sum('weight as weight')
        .from('productionTracking')
        .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
        .where({ 'productionTracking.isActive': true })
        .where('date', shift1Date)
        .andWhereBetween('time', ['06:00:00', '23:59:59'])
        .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
        .innerJoin(
          'subMaterial',
          'subMaterial.id',
          'productionTracking.subMaterialId'
        )
        .as('productionTracking');
    });

    const query2 = await db.from(function() {
      this.sum('weight as weight')
        .from('productionTracking')
        .select('productionTracking.subMaterialId', 'subMaterial.subMaterial')
        .where({ 'productionTracking.isActive': true })
        .where('date', shift2Date)
        .andWhereBetween('time', ['00:00:00', '05:59:59'])
        .groupBy('productionTracking.subMaterialId', 'subMaterial.subMaterial')
        .innerJoin(
          'subMaterial',
          'subMaterial.id',
          'productionTracking.subMaterialId'
        )
        .as('productionTracking');
    });

    if (query1) {
      await db.destroy();
      const data = query1.concat(query2);
      const totalWeight = data
        .reduce((x, y) => parseInt(x) + parseInt(y.weight), 0)
        .toLocaleString();
      return res.json({
        data: data.map(x => ({
          subMaterialId: x.subMaterialId,
          subMaterial: x.subMaterial,
          weight: roundToTwo(x.weight)
        })),
        totalWeight: totalWeight === 'NaN' ? 0 : totalWeight
      });
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

//total bags produced in month
router.get('/bags/byMonth1', async (req, res) => {
  let db = knex(config);
  try {
    db = knex(config);
    const d = new Date();
    const getMonth = 'extract(month from dates)';
    const getYear = 'extract(year from dates)';
    const currentMonth = parseInt(d.getMonth()) + 1;
    const currentYear = parseInt(d.getFullYear());
    const data = await db
      .select(db.raw(`sum(noofbags)`), 'dates')
      .from('MonthlyProdutionTracking')
      .where(db.raw(`${getYear}`), currentYear)
      .andWhere(db.raw(`${getMonth}`), currentMonth)
      .groupBy('dates');
    if (data) {
      await db.destroy();
      const monthData = data.map(x => ({
        day: moment(x.dates).format('Do MMM YYYY'),
        numberOfBags: x.sum
      }));
      return res.status(200).json({
        data: monthData,
        totalCount: monthData
          .reduce((x, y) => parseInt(x) + parseInt(y.numberOfBags), 0)
          .toLocaleString()
      });
    }
  } catch (error) {
    await db.destroy();
    return res.status(500).json({
      message: 'some thing awefull happened'
    });
  }
});

router.get('/bags/byMonth', (req, res) => {
  const db = knex(config);
  const d = new Date();
  const resolution = 'extract(day from "gs")';
  const getMonth = 'extract(month from "gs")';
  const getYear = 'extract(year from "gs")';

  const currentMonth = parseInt(d.getMonth()) + 1;
  const currentYear = parseInt(d.getFullYear());

  db.select(db.raw(`to_char(gs,'YYYY-MM-DD') as gs`))
    .from(
      db.raw(`generate_series(
      current_date - '31 day'::interval,
      current_date::timestamp,
      '1 day'::interval
    ) gs`)
    )
    .leftJoin('productionTracking', 'gs', 'productionTracking.date')
    .count('productionTracking.id')
    .where(db.raw(`${getYear}`), currentYear)
    .andWhere(db.raw(`${getMonth}`), currentMonth)
    .andWhere({ isActive: true })
    .groupBy('gs')
    .orderBy('gs')
    .then(x => {
      db.destroy();
      return res.status(200).json({
        data: x.map(x => ({
          day: moment(x.gs).format('Do MMM YYYY'),
          numberOfBags: x.count
        })),
        totalCount: x.reduce((x, y) => parseInt(x) + parseInt(y.count), 0)
      });
    })

    .catch(e => {
      db.destroy();

      return res.status(500).json(e);
    });
});

module.exports = router;
