const express = require('express');
const router = express.Router();
const knex = require('knex');
const config = require('../knexfile').development;
const t = require('tcomb-validation');
const validations = require('../utils/validations');
const moment = require('moment');
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const path = require('path');

const reqProductionPrint = (req, res, next) => {
    let result = t.validate(req.query, validations.printValidate);
    if (result.isValid()) next();
    else res.status(400).json(result.errors.map(x => x.message));
  };


        
  
router.get('/bags/print',reqProductionPrint, (req, res) => {
   
      const db = knex(config);
      const resolution = ' extract(hour from "time")';
      Array.prototype.groupBy = function(prop) {
        return this.reduce(function(groups, item) {
          const val = item[prop]
          groups[val] = groups[val] || []
          groups[val].push(item)
          return groups
        }, {})
      }
      var date=new Date(req.query.date);
        var dd=date.getDate();    
         date.setDate(dd+1);
         const shiftDate=moment(date).format('YYYY-MM-DD').toString()
        
    
      const query1 = db.select(
        db.raw(`${resolution} as "hour"`),'productionTracking.subMaterialId','subMaterial.subMaterial')
        .from('productionTracking')
        .where({'productionTracking.isActive': true })
        .andWhere({'productionTracking.date':req.query.date})
        .andWhereBetween('productionTracking.time',['06:00:00','23:59:59'])
        .groupByRaw(resolution)
        .groupBy('productionTracking.subMaterialId','subMaterial.subMaterial')
        .count('productionTracking.id')
        .innerJoin(
          'subMaterial',
          'subMaterial.id',
          'productionTracking.subMaterialId'
        )
    
        const query2 = db.select(
          db.raw(`${resolution} as "hour"`),'productionTracking.subMaterialId','subMaterial.subMaterial')
          .from('productionTracking')
          .where({'productionTracking.isActive': true })
          .andWhere({'productionTracking.date':shiftDate})
          .andWhereBetween('productionTracking.time',['00:00:00','05:59:59'])
          .groupByRaw(resolution)
          .groupBy('productionTracking.subMaterialId','subMaterial.subMaterial')
          .count('productionTracking.id')
          .innerJoin(
            'subMaterial',
            'subMaterial.id',
            'productionTracking.subMaterialId'
          )

          const query3=db.select('id').from('material')

          const promData = Promise.all([query1,query2,query3]);
      promData
        .then(results => {
         
          db.destroy();
          const shift1Data = [...results[0].filter(it => it.hour <18)];
          const shift2Data = [...results[0].filter(it => it.hour > 17), ...results[1]];
          const shift2 = shift2Data.groupBy('hour');
          const shift2Keys =[...Object.keys(shift2).filter(it => parseInt(it, 10) >= 18), ...Object.keys(shift2).filter(it => parseInt(it, 10) < 18)];
          const count1= shift1Data.groupBy('subMaterialId');
          const count2= shift2Data.groupBy('subMaterialId');
          const shift1Total = Object.keys(count1).map(key => ({
            subMaterialId: parseInt(key),
            count: count1[key].reduce((x, y) => x + parseInt(y.count, 10), 0)
          }));
         
         
          const shift2Total = Object.keys(count2).map(key => ({
           subMaterialId:parseInt(key),
           count:count2[key].reduce((x, y) => x + parseInt(y.count, 10), 0)
          }));
         console.log(shift2Total)
          const shift1SubCount=results[2].map(x => {
            const submaterial = shift1Total.find(it => it.subMaterialId === x.id)
            if(submaterial){
                return submaterial
            }
            else{
            return { subMaterialId:x.id,count: 0 }
            }
            });   
         const shift2SubCount=results[2].map(x => {
              const submaterial = shift2Total.find(it => it.subMaterialId === x.id)
              if(submaterial){
                  return submaterial
              }
              else{
              return { subMaterialId:x.id,count: 0 }
              }
              }); 
  
          const shift2O=shift2Keys.map(x => shift2[x])
          return res.status(200).json({
            shift1:shift1Data.groupBy('hour'),
            shift2:shift2Keys.map(x => shift2[x]) ,
            shift1SubCount: shift1SubCount.sort(function (a, b) {
              return a.subMaterialId - b.subMaterialId;
            }),
            shift2SubCount: shift2SubCount.sort(function (a, b) {
              return a.subMaterialId - b.subMaterialId;
            })  
          }); 
        })
        .catch(error => {
          db.destroy();
          return res.status(500).json({ message: "can't get values" });
        });
      });
  module.exports = router;
  