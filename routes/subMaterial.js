const express = require('express');
const router = express.Router();
const knex = require('knex');
const config = require('../knexfile').development;
const t = require('tcomb-validation');
const validations = require('../utils/validations');

router.get('/subMaterial', (req, res) => {
  const db = knex(config);
  db.select('id', 'subMaterial','materialId')
    .from('material')
    .where({ isActive: true })
    .then(data => {
const data1=data.filter(x=>x.subMaterial!=='').sort(function (a, b) {
  return a.id - b.id;
});

      db.destroy();
      res.status(200).json({ data: data1 });
    })
    .catch(e => {
      db.destroy();
      res.status(500).json(e.message);
    });
});

//to update submaterial table
router.get('/updatesubmaterial', async (req, res) => {
  let db;
  try{
    db = knex(config);
   const getMaterial=await db.select('id','materialId', 'subMaterial')
    .from('material')
    .andWhere('materialId',req.query.materialId)
console.log(getMaterial[0].materialId,getMaterial[0].subMaterial)
    const updatematerial=await db.select('*')
    .from('subMaterial')
    .update({
         materialId:getMaterial[0].materialId,
         subMaterial:getMaterial[0].subMaterial
    })
    .where('id',req.query.id)
    .returning('*')

    await db.destroy();
    if(getMaterial){
    res.status(200).json(updatematerial);
  }
  
}
  catch(error){
    await db.destroy();
       return res.status(500).json(error)
  }
     
});

//devices active and inactive
router.get('/devicesStatus', async(req, res) => {
  const db = knex(config);
  const d = new Date();
  Array.prototype.unique = function() {
    var arr = [];
    for(var i = 0; i < this.length; i++) {
        if(!arr.includes(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr; 
}

    const currentDay = d.getDate();
    const currentMonth = parseInt(d.getMonth()) + 1;
    const currentYear = parseInt(d.getFullYear());
    const currentDate=currentYear+'-'+currentMonth+'-'+currentDay
 const devices=await db.select('machineId')
    .from('productionTracking')
    .where({ isActive: true })
    .andWhere('date',currentDate)
    .groupBy('machineId')

    if(devices){
      const active=devices.map(x=>x.machineId.split('')[0]).unique().length
      const inactive=4-active;
      return res.status(200).json({
        activeDevices:active,
        inactiveDevices:inactive
       })
    }
    else{
      return res.json({message:'not found'})
    }
});

router.get('/logtracking', async(req, res) => {
  let db 
  try{
  db = knex(config);
  const log=await db.select('id','log').from('logTracking').orderBy('created_at','desc')
  await db.destroy();
  if(log[0]){
    return res.status(200).json(log.map(x=>x.log))
  }
  else{
    return res.json('no log data')
  }
}
catch(error){
  await db.destroy();
  return res.json('something wrong')
}
});

module.exports = router;
