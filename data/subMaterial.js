
module.exports = [
  { id:1, materialId: 1,machineId: 'A1', subMaterial: 'STG RAWA', isActive: true },
  { id:2, materialId: 2, machineId: 'A2', subMaterial: 'BELL RAWA', isActive: true },
  { id:3, materialId: 3, machineId: 'A3',subMaterial: 'BELL CHIROTY', isActive: true },
  { id:4, materialId: 4, machineId: 'A4', subMaterial: 'G ATTA', isActive: true },
  { id:5, materialId: 5,  machineId: 'B1',subMaterial: 'BELL ROUGH BRAN 49KG', isActive: true },
  { id:6, materialId: 6, machineId: 'B2', subMaterial: 'BELL SUPERFINE 49KG', isActive: true },
  { id:7,materialId: 7, machineId: 'B3',subMaterial: 'CG SUPERFINE 49KG', isActive: true },
  { id:8,materialId: 8, machineId: 'B4',subMaterial:'CG SUPER DELUXE 49KG', isActive: true },
  { id:9,materialId: 9, machineId: 'B5',subMaterial: 'CG SUPER DELUXE 45KG', isActive: true },
  { id:10,materialId: 10,machineId: 'B6',subMaterial: 'CG FLAKES 34KG', isActive: true },
  { id:11,materialId: 11, machineId: 'B7',subMaterial: 'CG FLAKES 30KG', isActive: true },
  { id:12,materialId: 12, machineId: 'C1',subMaterial: 'STG ATTA', isActive: true },
  { id:13,materialId: 13, machineId: 'C2',subMaterial: 'BELL ATTA', isActive: true },
  { id:14,materialId: 14,machineId: 'C3', subMaterial: 'STG CHAKKI 50KG', isActive: true },
  { id:15,materialId: 15,machineId: 'C4', subMaterial: 'STG CHAKKI 25KG', isActive: true },
  { id:16,materialId: 16, machineId: 'C5',subMaterial: 'ST CHAKKI 50KG', isActive: true },
  { id:17,materialId: 17, machineId: 'C6',subMaterial: 'ST CHAKKI 25KG', isActive: true },
  { id:18,materialId: 18, machineId: 'D1',subMaterial: 'STG MAIDA', isActive: true },
  { id:19,materialId: 19, machineId: 'D2',subMaterial: 'BREAD MAIDA', isActive: true },
  { id:20,materialId: 20, machineId: 'D3',subMaterial: 'G MAIDA', isActive: true },
  {id:21, materialId: 21,machineId: 'A5', subMaterial: '', isActive: true },
  { id:22, materialId: 22, machineId: 'A6', subMaterial: '', isActive: true },
  { id:23, materialId: 23, machineId: 'A7',subMaterial: '', isActive: true },
  { id:24, materialId: 24, machineId: 'C7', subMaterial: '', isActive: true },
  { id:25, materialId: 25,  machineId: 'D4',subMaterial: '', isActive: true },
  { id:26, materialId: 26, machineId: 'D5', subMaterial: '', isActive: true },
  { id:27,materialId: 27, machineId: 'D6',subMaterial: '', isActive: true },
  { id:28,materialId: 28, machineId: 'D7',subMaterial:'', isActive: true },
];