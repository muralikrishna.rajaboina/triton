exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('material', function(table) {
      table.increments('id').primary();
      table.integer('materialId')
      table.string('materialName');
      table.boolean('isActive');
      table
        .timestamp('created_at')
        .notNullable()
        .defaultTo(knex.fn.now());
      table
        .timestamp('updated_at')
        .notNullable()
        .defaultTo(knex.fn.now());
    })
    .createTable('subMaterial', function(table) {
      table.increments('id').primary();
      table.integer('materialId')
      table.string('machineId')
      table.string('subMaterial');
      table.boolean('isActive');
    });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('subMaterial').dropTable('material');
};
