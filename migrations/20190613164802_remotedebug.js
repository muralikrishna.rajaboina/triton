exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('remoteDebug', function(table) {
      table.increments('id').primary();
      table.string('deviceId');
      table.string('systemRestartState');
      table.string('taskNumberState');
      table.string('stackOverflowstate');
      table.string('hardFault');
      table
        .timestamp('created_at')
        .notNullable()
        .defaultTo(knex.fn.now());
      table
        .timestamp('updated_at')
        .notNullable()
        .defaultTo(knex.fn.now());
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('remoteDebug')]);
};
