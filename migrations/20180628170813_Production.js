exports.up = function(knex, Promise) {
  return knex.schema.createTable('productionTracking', function(table) {
    table.increments('id').primary();
    table
      .integer('subMaterialId')
      .references('id')
      .inTable('subMaterial');
    table.double('weight');
    table.string('machineId');
    table.string('uniqueProductId');
    table.date('date');
    table.time('time');
    table.boolean('isActive');
    table
      .timestamp('created_at')
      .notNullable()
      .defaultTo(knex.fn.now());
    table
      .timestamp('updated_at')
      .notNullable()
      .defaultTo(knex.fn.now());
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('Production');
};
