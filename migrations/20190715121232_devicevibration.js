exports.up = function(knex, Promise) {
  return knex.schema.createTable('devicevibration', function(table) {
    table.increments('id').primary();
    table.double('temperature');
    table.string('vibration');
    table.date('date');
    table.time('time');
    table.boolean('isActive');
    table
      .timestamp('created_at')
      .notNullable()
      .defaultTo(knex.fn.now());
    table
      .timestamp('updated_at')
      .notNullable()
      .defaultTo(knex.fn.now());
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('devicevibration');
};
