const t = require('tcomb');
const moment = require('moment');

const validateRole = t.struct({
  roleName: t.String,
});
const validateRequest = t.struct({
  firstName: t.String,
  lastName: t.String,
  emailId: t.String,
  password: t.String,
  password2: t.String,
  roleName: t.String,
  
});

const login = t.struct({
  emailId: t.String,
  password: t.String
});

const productionValidate = t.struct({
  subMaterialId: t.Integer,
  weight: t.Number,
  machineId: t.String,
  date: t.refinement(
    t.String,
    s => moment(s, 'YYYY-MM-DD', true).isValid(),
    'date format should be YYYY-MM-DD'
  ),
  time: t.refinement(
    t.String,
    s => moment(s, 'HH:mm:ss', true).isValid(),
    'time format should be HH:mm:ss'
  )
});
const StringNumber = t.refinement(t.String, s => !isNaN(s));
const productionGetValidate = t.struct({
  subMaterialId: StringNumber,
  weight:StringNumber,
  machineId: t.String,
  date: t.refinement(
    t.String,
    s => moment(s, 'YYYY-MM-DD', true).isValid(),
    'date format should be YYYY-MM-DD'
  ),
  time: t.refinement(
    t.String,
    s => moment(s, 'HH:mm:ss', true).isValid(),
    'time format should be HH:mm:ss'
  )
});


const pagination = t.struct({
  pageSize: StringNumber,
  pageIndex: StringNumber
});
const deleteValidate=t.struct({id:StringNumber})

const graphValidate = t.struct({
  fromDate: t.refinement(
    t.String,
    s => moment(s, 'YYYY-MM-DD', true).isValid(),
    'date format should be YYYY-MM-DD'
  ),
  toDate: t.refinement(
    t.String,
    s => moment(s, 'YYYY-MM-DD', true).isValid(),
    'date format should be YYYY-MM-DD'
  ),
  subMaterialId: StringNumber
});

const bagsCurrentDayValidate = t.struct({
  subMaterialId: StringNumber
});
const printValidate = t.struct({
  date: t.refinement(
    t.String,
    s => moment(s, 'YYYY-MM-DD', true).isValid(),
    'date format should be YYYY-MM-DD'
  )
});
module.exports = {
  validateRole,
  validateRequest,
  login,
  productionValidate,
  productionGetValidate,
  pagination,
  graphValidate,
  bagsCurrentDayValidate,
  deleteValidate,
  printValidate
};
