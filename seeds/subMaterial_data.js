const materialData = require('../data/material');
const subMaterialData = require('../data/subMaterial');
const config = require('../knexfile').development;

exports.seed = function(knex, Promise) {
  const db = knex(config)
  return  db
  .from('material')
  .insert(materialData)
  .then((data) => {
     return db
    .from('subMaterial')
    .insert(subMaterialData)
    .then(res => res)
    });
};
